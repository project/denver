<?php

if (is_null(theme_get_setting('denver_style'))) {
  global $theme_key;
  // Save default theme settings
  $defaults = array(

    'denver_suckerfish' => 0,
        'denver_suckerfishcolor' => 'blue',
    'denver_bodybg' => 'bodybg-gradient',
        'denver_custombodybg' => '',
    'denver_font' => 'font-arial-verdana-sansserif',
        'denver_customfont' => '',        
    'denver_width' => 'width-80',
        'denver_customwidth' => '',
    'denver_padding' => 'padding-20px-0',
        'denver_custompadding' => '',
    'denver_pgheader' => 'pgheader-tealgradient',
        'denver_custompgheader' => '',
    'denver_pgheaderh' => 'pgheaderh-auto',
        'denver_custompgheaderh' => '',
    'denver_logohalign' => 'logohalign-left',
    'denver_navbg' => 'navbg-silvergradient',
        'denver_customnavbg' => '',
    'denver_ptabsposition' => 'ptabs-navleft',
    'denver_ptabs' => 'ptabs-blue',
        'denver_customptabs' => '',
        'denver_customptabsah' => '',
        'denver_customptabsaa' => '',
    'denver_plinksa' => 'plinksa-gray',
        'denver_customplinksa' => '',
        'denver_customplinksah' => '',
        'denver_customplinksaa' => '',
    'denver_searchposition' => 'search-navright',
    'denver_secposition' => 'secondary-center',
    'denver_secbg' => 'secbg-gray',
        'denver_customsecbg' => '',
    'denver_pgbg' => 'pgbg-lightgradient',
        'denver_custompgbg' => '',
    'denver_centerbg' => 'centerbg-trans',
        'denver_customcenterbg' => '',
    'denver_pgcontentbg' => 'pgcontentbg-lightgradient',
        'denver_custompgcontentbg' => '',
    'denver_mainbg' => 'mainbg-trans',
        'denver_custommainbg' => '',
    'denver_nodebg' => 'nodebg-trans',
        'denver_customnodebg' => '',
    'denver_nodecontentbg' => 'nodecontentbg-trans',
        'denver_customnodecontentbg' => '',
    'denver_leftsidebarwidth' => 'leftsidebarwidth-200',
        'denver_customleftsidebarwidth' => '',
    'denver_rightsidebarwidth' => 'rightsidebarwidth-200',
        'denver_customrightsidebarwidth' => '',
    'denver_leftbg' => 'leftbg-trans',
        'denver_customleftbg' => '',
    'denver_blocklbg' => 'blocklbg-trans',
        'denver_customblocklbg' => '',
    'denver_blockltitle' => 'blockltitle-blue',
        'denver_customblockltitle' => '',
    'denver_blocklcontent' => 'blocklcontent-lightgray',
        'denver_customblocklcontent' => '',
    'denver_rightbg' => 'rightbg-trans',
        'denver_customrightbg' => '',
    'denver_blockrbg' => 'blockrbg-trans',
        'denver_customblockrbg' => '',
    'denver_blockrtitle' => 'blockrtitle-blue',
        'denver_customblockrtitle' => '',
    'denver_blockrcontent' => 'blockrcontent-lightgray',
        'denver_customblockrcontent' => '',
    'denver_footerbg' => 'footerbg-darkgray',
        'denver_customfooterbg' => '',
    'denver_style' => 'blue',
    'denver_cssfile' => 0,
        'denver_customcssfile' => '',
    'denver_csstype' => 0,
        'denver_customcsstype' => '',
    'denver_jsfile' => 0,
        'denver_customjsfile' => '',
    'denver_jstype' => 0,
        'denver_customjstype' => '',
    'denver_breadcrumb' => 0,
    'denver_iepngfix' => 1,
    'denver_themelogo' => 0,
  );
  variable_set(
    str_replace('/', '_', 'theme_'. $theme_key .'_settings'),
    array_merge(theme_get_settings($theme_key), $defaults)
  );
  // Force refresh of Drupal internals
  theme_get_setting('', TRUE);
}

$bodybg = theme_get_setting('denver_bodybg');
if (!$bodybg)
{
   $bodybg = '';
}

$font = theme_get_setting('denver_font');
if (!$font)
{
   $font = '';
}

$width = theme_get_setting('denver_width');
if (!$width)
{
   $width = '';
}

$padding = theme_get_setting('denver_padding');
if (!$padding)
{
   $padding = '';
}

$pgheader = theme_get_setting('denver_pgheader');
if (!$pgheader)
{
   $pgheader = '';
}

$pgheaderh = theme_get_setting('denver_pgheaderh');
if (!$pgheaderh)
{
   $pgheaderh = '';
}

$logohalign = theme_get_setting('denver_logohalign');
if (!$logohalign)
{
   $logohalign = '';
}

$navbg = theme_get_setting('denver_navbg');
if (!$navbg)
{
   $navbg = '';
}

$ptabsposition = theme_get_setting('denver_ptabsposition');
if (!$ptabsposition)
{
   $ptabsposition = '';
}

$ptabs = theme_get_setting('denver_ptabs');
if (!$ptabs)
{
   $ptabs = '';
}

$plinksa = theme_get_setting('denver_plinksa');
if (!$plinksa)
{
   $plinksa = '';
}

$searchposition = theme_get_setting('denver_searchposition');
if (!$searchposition)
{
   $searchposition = '';
}

$secposition = theme_get_setting('denver_secposition');
if (!$secposition)
{
   $secposition = '';
}

$secbg = theme_get_setting('denver_secbg');
if (!$secbg)
{
   $secbg = '';
}

$pgbg = theme_get_setting('denver_pgbg');
if (!$pgbg)
{
   $pgbg = '';
}

$centerbg = theme_get_setting('denver_centerbg');
if (!$centerbg)
{
   $centerbg = '';
}

$pgcontentbg = theme_get_setting('denver_pgcontentbg');
if (!$pgcontentbg)
{
   $pgcontentbg = '';
}

$mainbg = theme_get_setting('denver_mainbg');
if (!$mainbg)
{
   $mainbg = '';
}

$nodebg = theme_get_setting('denver_nodebg');
if (!$nodebg)
{
   $nodebg = '';
}

$nodecontentbg = theme_get_setting('denver_nodecontentbg');
if (!$nodecontentbg)
{
   $nodecontentbg = '';
}

$leftsidebarwidth = theme_get_setting('denver_leftsidebarwidth');
if (!$leftsidebarwidth)
{
   $leftsidebarwidth = '';
}

$rightsidebarwidth = theme_get_setting('denver_rightsidebarwidth');
if (!$rightsidebarwidth)
{
   $rightsidebarwidth = '';
}

$leftbg = theme_get_setting('denver_leftbg');
if (!$leftbg)
{
   $leftbg = '';
}

$blocklbg = theme_get_setting('denver_blocklbg');
if (!$blocklbg)
{
   $blocklbg = '';
}

$blockltitle = theme_get_setting('denver_blockltitle');
if (!$blockltitle)
{
   $blockltitle = '';
}

$blocklcontent = theme_get_setting('denver_blocklcontent');
if (!$blocklcontent)
{
   $blocklcontent = '';
}

$rightbg = theme_get_setting('denver_rightbg');
if (!$rightbg)
{
   $rightbg = '';
}

$blockrbg = theme_get_setting('denver_blockrbg');
if (!$blockrbg)
{
   $blockrbg = '';
}

$blockrtitle = theme_get_setting('denver_blockrtitle');
if (!$blockrtitle)
{
   $blockrtitle = '';
}

$blockrcontent = theme_get_setting('denver_blockrcontent');
if (!$blockrcontent)
{
   $blockrcontent = '';
}

$footerbg = theme_get_setting('denver_footerbg');
if (!$footerbg)
{
   $footerbg = '';
}

$style = theme_get_setting('denver_style');
if (!$style)
{
   $style = '';
}

$suckerfishcolor = theme_get_setting('denver_suckerfishcolor');
if (!$suckerfishcolor)
{
   $suckerfishcolor = '';
}

if (isset($_COOKIE["denverstyle"])) {
   $style = $_COOKIE["denverstyle"];
}

if (theme_get_setting('denver_suckerfish')) {
   drupal_add_js(drupal_get_path('theme', 'denver') . '/js/suckerfish.js', 'theme');
   drupal_add_css(drupal_get_path('theme', 'denver') . '/css/suckerfish_'  . $suckerfishcolor . '.css', 'theme');
}

drupal_add_css(drupal_get_path('theme', 'denver') . '/css/' . $style . '.css', 'theme');

drupal_add_css(drupal_get_path('theme', 'denver') . '/css/' . $bodybg . '.css', 'theme');

drupal_add_css(drupal_get_path('theme', 'denver') . '/css/' . $font . '.css', 'theme');

drupal_add_css(drupal_get_path('theme', 'denver') . '/css/' . $width . '.css', 'theme');

drupal_add_css(drupal_get_path('theme', 'denver') . '/css/' . $padding . '.css', 'theme');

drupal_add_css(drupal_get_path('theme', 'denver') . '/css/' . $pgheader . '.css', 'theme');

drupal_add_css(drupal_get_path('theme', 'denver') . '/css/' . $pgheaderh . '.css', 'theme');

drupal_add_css(drupal_get_path('theme', 'denver') . '/css/' . $logohalign . '.css', 'theme');

drupal_add_css(drupal_get_path('theme', 'denver') . '/css/' . $navbg . '.css', 'theme');

drupal_add_css(drupal_get_path('theme', 'denver') . '/css/' . $ptabsposition . '.css', 'theme');

drupal_add_css(drupal_get_path('theme', 'denver') . '/css/' . $ptabs . '.css', 'theme');

drupal_add_css(drupal_get_path('theme', 'denver') . '/css/' . $plinksa . '.css', 'theme');

drupal_add_css(drupal_get_path('theme', 'denver') . '/css/' . $searchposition . '.css', 'theme');

drupal_add_css(drupal_get_path('theme', 'denver') . '/css/' . $secposition . '.css', 'theme');

drupal_add_css(drupal_get_path('theme', 'denver') . '/css/' . $secbg . '.css', 'theme');

drupal_add_css(drupal_get_path('theme', 'denver') . '/css/' . $pgbg . '.css', 'theme');

drupal_add_css(drupal_get_path('theme', 'denver') . '/css/' . $centerbg . '.css', 'theme');

drupal_add_css(drupal_get_path('theme', 'denver') . '/css/' . $pgcontentbg . '.css', 'theme');

drupal_add_css(drupal_get_path('theme', 'denver') . '/css/' . $mainbg . '.css', 'theme');

drupal_add_css(drupal_get_path('theme', 'denver') . '/css/' . $nodebg . '.css', 'theme');

drupal_add_css(drupal_get_path('theme', 'denver') . '/css/' . $nodecontentbg . '.css', 'theme');

drupal_add_css(drupal_get_path('theme', 'denver') . '/css/' . $leftsidebarwidth . '.css', 'theme');

drupal_add_css(drupal_get_path('theme', 'denver') . '/css/' . $rightsidebarwidth . '.css', 'theme');

drupal_add_css(drupal_get_path('theme', 'denver') . '/css/' . $leftbg . '.css', 'theme');

drupal_add_css(drupal_get_path('theme', 'denver') . '/css/' . $blocklbg . '.css', 'theme');

drupal_add_css(drupal_get_path('theme', 'denver') . '/css/' . $blockltitle . '.css', 'theme');

drupal_add_css(drupal_get_path('theme', 'denver') . '/css/' . $blocklcontent . '.css', 'theme');

drupal_add_css(drupal_get_path('theme', 'denver') . '/css/' . $rightbg . '.css', 'theme');

drupal_add_css(drupal_get_path('theme', 'denver') . '/css/' . $blockrbg . '.css', 'theme');

drupal_add_css(drupal_get_path('theme', 'denver') . '/css/' . $blockrtitle . '.css', 'theme');

drupal_add_css(drupal_get_path('theme', 'denver') . '/css/' . $blockrcontent . '.css', 'theme');

drupal_add_css(drupal_get_path('theme', 'denver') . '/css/' . $footerbg . '.css', 'theme');

if (theme_get_setting('denver_themelogo'))
{
   function _phptemplate_variables($hook, $variables = array()) {
     $variables['logo'] = base_path() . path_to_theme() . '/css/' . theme_get_setting('denver_style') . '/logo.gif';
     return $variables;
   }
}

if (theme_get_setting('denver_iepngfix')) {
   drupal_add_js(drupal_get_path('theme', 'denver') . '/js/jquery.pngFix.js', 'theme');
}

function phptemplate_linksnew($links, $attributes = array('class' => 'links')) {
  $output = '';
  if (count($links) > 0) {
    $output = '<ul'. drupal_attributes($attributes) .'>';
    $num_links = count($links);
    $i = 1;
    foreach ($links as $key => $link) {
      $class = '';
      // Automatically add a class to each link and also to each LI
      if (isset($link['attributes']) && isset($link['attributes']['class'])) {
        $link['attributes']['class'] .= ' ' . $key;
        $class = $key;
      }
      else {
        $link['attributes']['class'] = $key;
        $class = $key;
      }
      // Add first and last classes to the list of links to help out themers.
      $extra_class = '';
      if ($i == 1) {
        $extra_class .= 'first ';
      }
      if ($i == $num_links) {
        $extra_class .= 'last ';
      }
	  // Add class active to active li 
	  $current = '';
	  if (strstr($class, 'active')) {
	    $current = ' active';
	  }	
	  $output .= '<li class="'. $extra_class . $class . $current .'"><span>';
	  // Is the title HTML?
      $html = isset($link['html']) && $link['html'];
      // Initialize fragment and query variables.
      $link['query'] = isset($link['query']) ? $link['query'] : NULL;
      $link['fragment'] = isset($link['fragment']) ? $link['fragment'] : NULL;
      if (isset($link['href'])) {
        $output .= l($link['title'], $link['href'], $link['attributes'], $link['query'], $link['fragment'], FALSE, $html);
      }
      else if ($link['title']) {
        //Some links are actually not links, but we wrap these in <span> for adding title and class attributes
        if (!$html) {
          $link['title'] = check_plain($link['title']);
        }
        $output .= '<span'. drupal_attributes($link['attributes']) .'>'. $link['title'] .'</span>';
      }
      $i++;
      $output .= "</span></li>\n";
    }
    $output .= '</ul>';
  }
  return $output;
}

//  Breadcrumb override
function denver_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
	  $breadcrumb[] = drupal_get_title();  // RADUT's complete breadcrumb ( � = › , � = &#187; &raquo;)
    return '<div class="breadcrumb">'. implode(' &raquo; ', $breadcrumb) .'</div>';
  }
}

//  get the description displayed when hovering over a menu item
function menu_get_active_description() {
  if ($mid = menu_get_active_nontask_item()) {
    $item = menu_get_item($mid);
    return $item['description'];
  }
} 

// Quick fix for the validation error: 'ID "edit-submit" already defined'
$elementCountForHack = 0;
function phptemplate_submit($element) {
  global $elementCountForHack;
  return str_replace('edit-submit', 'edit-submit-' . ++$elementCountForHack, theme('button', $element));
}

function denver_regions() {
  return array(
       'page_top' => t('page top'),
       'header' => t('header'),
       'header_right' => t('header right'),
       'suckerfish_menu' => t('suckerfish menu'),
       'sub_nav' => t('sub nav'),
       'node_top' => t('node top'),
       'sidebar_left' => t('left sidebar'),
       'sidebar_right' => t('right sidebar'),
       'content_top' => t('content top'),
       'user1' => t('user1'),
       'user2' => t('user2'),
       'user3' => t('user3'),
       'user4' => t('user4'),
       'user5' => t('user5'),
       'user6' => t('user6'),
       'content_bottom' => t('content bottom'),
       'footer' => t('footer'),
       'page_bottom' => t('page bottom')
  );
}

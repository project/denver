<?php

function phptemplate_settings($saved_settings) {

  $settings = theme_get_settings('denver');

  $defaults = array(
    'denver_bodybg' => '',
        'denver_custombodybg' => '',
    'denver_font' => '',
        'denver_customfont' => '',        
    'denver_width' => '',
        'denver_customwidth' => '',
    'denver_padding' => '',
        'denver_custompadding' => '',
    'denver_pgheader' => '',
        'denver_custompgheader' => '',
    'denver_pgheaderh' => '',
        'denver_custompgheaderh' => '',
    'denver_logohalign' => '',        
    'denver_navbg' => '',
        'denver_customnavbg' => '',
    'denver_ptabsposition' => '',
    'denver_ptabs' => '',
        'denver_customptabs' => '',
        'denver_customptabsah' => '',
        'denver_customptabsaa' => '',
    'denver_plinksa' => '',
        'denver_customplinksa' => '',
        'denver_customplinksah' => '',
        'denver_customplinksaa' => '',
    'denver_searchposition' => '',
    'denver_secposition' => '',
    'denver_secbg' => '',
        'denver_customsecbg' => '',
    'denver_pgbg' => '',
        'denver_custompgbg' => '',
    'denver_centerbg' => '',
        'denver_customcenterbg' => '',
    'denver_pgcontentbg' => '',
        'denver_custompgcontentbg' => '',
    'denver_mainbg' => '',
        'denver_custommainbg' => '',
    'denver_nodebg' => '',
        'denver_customnodebg' => '',
    'denver_nodecontentbg' => '',
        'denver_customnodecontentbg' => '',
    'denver_leftsidebarwidth' => '',
        'denver_customleftsidebarwidth' => '',
    'denver_rightsidebarwidth' => '',
        'denver_customrightsidebarwidth' => '',      
    'denver_leftbg' => '',
        'denver_customleftbg' => '',
    'denver_blocklbg' => '',
        'denver_customblocklbg' => '',
    'denver_blockltitle' => '',
        'denver_customblockltitle' => '',
    'denver_blocklcontent' => '',
        'denver_customblocklcontent' => '',
    'denver_rightbg' => '',
        'denver_customrightbg' => '',
    'denver_blockrbg' => '',
        'denver_customblockrbg' => '',
    'denver_blockrtitle' => '',
        'denver_customblockrtitle' => '',
    'denver_blockrcontent' => '',
        'denver_customblockrcontent' => '',
    'denver_footerbg' => '',
        'denver_customfooterbg' => '',
    'denver_style' => '',
    'denver_cssfile' => 0,
        'denver_customcssfile' => '',
    'denver_csstype' => 0,
        'denver_customcsstype' => '',
    'denver_jsfile' => 0,
        'denver_customjsfile' => '',
    'denver_jstype' => 0,
        'denver_customjstype' => '',
    'denver_breadcrumb' => 0,
    'denver_iepngfix' => 1,
    'denver_themelogo' => 0,
    'denver_suckerfish' => 0,
        'denver_suckerfishcolor' => '', 
  );

  $settings = array_merge($defaults, $settings);
  $form['denver_style'] = array(
    '#type' => 'select',
    '#title' => t('Style'),
    '#default_value' => $settings['denver_style'],
    '#options' => array(
      'blue' => t('Blue (default)'),
    ),
  );

  $settings = array_merge($defaults, $settings);
  $form['denver_bodybg'] = array(
    '#type' => 'select',
    '#title' => t('Body Background'),
    '#default_value' => $settings['denver_bodybg'],
    '#options' => array(
      'bodybg-gradient' => t('Gray Gradient (default)'),
      'bodybg-teal-gradient' => t('Teal Gradient'),
      'bodybg-white' => t('White'),
      'bodybg-light-gray' => t('Light Gray'),
      'bodybg-dark-gray' => t('Dark Gray'),
      'bodybg-black' => t('Black'),
      'bodybg-none' => t('None'),
      'bodybg-custom-color' => t('Custom Body Background Color'),
      'bodybg-custom' => t('Custom Body Background'),
    ),
  );

  $form['denver_custombodybg'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom Body Background (must select Custom Body Background above) - i.e. #000000, red, #ffffff url(http://mysite.com/images/body-bg.gif) 0 0 repeat-x, etc. Using <i>Custom Body Background Color</i> will keep the rounded top corners. Using <i>Custom Body Background</i> will remove the rounded top corners. Use regular css with full URL to image'),
    '#default_value' => $settings['denver_custombodybg'],
    '#size' => 40,
    '#maxlength' => 100,
  );

  $form['denver_font'] = array(
    '#type' => 'select',
    '#title' => t('Font Family'),
    '#default_value' => $settings['denver_font'],
    '#options' => array(
     'font-arial-verdana-sansserif' => t('Arial, Verdana, sans-serif (Default)'),
     'font-arialnarrow-arial-helvetica-sansserif' => t('"Arial Narrow", Arial, Helvetica, sans-serif'),
     'font-timesnewroman-times-serif' => t('"Times New Roman", Times, serif'),
     'font-lucidasans-verdana-arial-sansserif' => t('"Lucida Sans", Verdana, Arial, sans-serif'),
     'font-lucidagrande-verdana-sansserif' => t('"Lucida Grande", Verdana, sans-serif'),
     'font-tahoma-verdana-arial-helvetica-sansserif' => t('Tahoma, Verdana, Arial, Helvetica, sans-serif'),
     'font-georgia-timesnewroman-times-serif' => t('Georgia, "Times New Roman", Times, serif'),
     'font-custom' => t('Custom Font (specify below)'),
    ),
  );

  $form['denver_customfont'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom Font-Family Setting'),
    '#default_value' => $settings['denver_customfont'],
    '#size' => 40,
    '#maxlength' => 100,
  );

  $settings = array_merge($defaults, $settings);
  $form['denver_width'] = array(
    '#type' => 'select',
    '#title' => t('Page Width'),
    '#default_value' => $settings['denver_width'],
    '#options' => array(
      'width-80' => t('80% (default)'),
      'width-100' => t('100%'),
      'width-700px' => t('700px'),
      'width-850px' => t('850px'),
      'width-custom' => t('Custom Page Width'),
    ),
  );

  $form['denver_customwidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom Page Width in pixels or percent (must select Custom Page Width above) - i.e. 850px, 95%, etc'),
    '#default_value' => $settings['denver_customwidth'],
    '#size' => 6,
    '#maxlength' => 6,
  );

  $settings = array_merge($defaults, $settings);
  $form['denver_padding'] = array(
    '#type' => 'select',
    '#title' => t('Body Padding'),
    '#default_value' => $settings['denver_padding'],
    '#options' => array(
      'padding-20px-0' => t('20px 0 (default)'),
      'padding-20px' => t('20px'),
      'padding-5em-0' => t('5em 0'),
      'padding-5em' => t('5em'),
      'padding-0' => t('0 (None)'),
      'padding-custom' => t('Custom Body Padding'),
    ),
  );

  $form['denver_custompadding'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom Body Padding in pixels or em (must select Custom Body Padding above) - i.e. 10px 0, 10px, 3em 0, 3em, 20px 40px 5px, etc. Use regular css'),
    '#default_value' => $settings['denver_custompadding'],
    '#size' => 27,
    '#maxlength' => 27,
  );

  $settings = array_merge($defaults, $settings);
  $form['denver_pgheader'] = array(
    '#type' => 'select',
    '#title' => t('Header Background'),
    '#default_value' => $settings['denver_pgheader'],
    '#options' => array(
      'pgheader-tealgradient' => t('Teal Gradient (default)'),
      'pgheader-trans' => t('Transparent'),
      'pgheader-none' => t('No Page Header'),
      'pgheader-custom-color' => t('Custom Header Color'),
      'pgheader-custom' => t('Custom Header Background'),
    ),
  );

  $form['denver_custompgheader'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom Header Background (must select Custom Header Background above) - i.e. #000, red, #fff url(http://mysite.com/images/header-bg.gif) 0 0 repeat-x, etc. Using <i>Custom Header Color</i> will keep the rounded top corners. Using <i>Custom Header Background</i> will remove the rounded top corners. Use regular css with full URL to image'),
    '#default_value' => $settings['denver_custompgheader'],
    '#size' => 40,
    '#maxlength' => 300,
  );
  
  $settings = array_merge($defaults, $settings);
  $form['denver_pgheaderh'] = array(
    '#type' => 'select',
    '#title' => t('Header Height'),
    '#default_value' => $settings['denver_pgheaderh'],
    '#options' => array(
      'pgheaderh-auto' => t('Auto (default)'),
      'pgheaderh-custom' => t('Custom Header Height'),
    ),
  );

  $form['denver_custompgheaderh'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom Header Height (must select Custom Header Height above) - i.e. 60px, 10em, etc.'),
    '#default_value' => $settings['denver_custompgheaderh'],
    '#size' => 6,
    '#maxlength' => 6,
  );
  
  $settings = array_merge($defaults, $settings);
  $form['denver_logohalign'] = array(
    '#type' => 'select',
    '#title' => t('Logo Horizontal Alignment'),
    '#default_value' => $settings['denver_logohalign'],
    '#options' => array(
      'logohalign-left' => t('Left (default)'),
      'logohalign-center' => t('Center'),
      'logohalign-right' => t('Right'),
    ),
  );

  $settings = array_merge($defaults, $settings);
  $form['denver_navbg'] = array(
    '#type' => 'select',
    '#title' => t('Navigation Area'),
    '#default_value' => $settings['denver_navbg'],
    '#options' => array(
      'navbg-silvergradient' => t('Silver Gradient (default)'),
      'navbg-blackgradient' => t('Black Gradient'),
      'navbg-blackshiny' => t('Shiny Black'),
      'navbg-trans' => t('Transparent'),
      'navbg-none' => t('No Navigation Area'),
      'navbg-custom' => t('Custom Navigation Background'),
      'navbg-customgradient' => t('Gradient Overlay (Set Background Color Below)'),
    ),
  );

  $form['denver_customnavbg'] = array(
    '#type' => 'textfield',
    '#title' => t('Navigation Background (must use <i>Custom Navigation Background</i> or <i>Gradient Overlay</i> above) - i.e. #000000, red, #ffffff url(http://mysite.com/images/nav-bg.gif) 0 0 repeat-x, etc. Use regular css with full URL to image'),
    '#default_value' => $settings['denver_customnavbg'],
    '#size' => 40,
    '#maxlength' => 100,
  );

  $settings = array_merge($defaults, $settings);
  $form['denver_ptabsposition'] = array(
    '#type' => 'select',
    '#title' => t('Tabs Position'),
    '#default_value' => $settings['denver_ptabsposition'],
    '#options' => array(
      'ptabs-navleft' => t('Left Navigation Area (default)'),
      'ptabs-navright' => t('Right Navigation Area'),
      'ptabs-headleft' => t('Left Header Area'),
      'ptabs-headright' => t('Right Header Area'),
    ),
  );

  $settings = array_merge($defaults, $settings);
  $form['denver_ptabs'] = array(
    '#type' => 'select',
    '#title' => t('Tabs'),
    '#default_value' => $settings['denver_ptabs'],
    '#options' => array(
      'ptabs-blue' => t('Blue Tabs (default)'),
      'ptabs-gold' => t('Gold Tabs'),
      'ptabs-green' => t('Green Tabs'),
      'ptabs-red' => t('Red Tabs'),
      'ptabs-teal' => t('Teal Tabs'),
      'ptabs-custom-color' => t('Custom Tabs Color'),
      'ptabs-custom' => t('Custom Tabs Background'),
    ),
  );

  $form['denver_customptabs'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom Tabs (must select <i>Custom Tabs Color</i> or <i>Custom Tabs Background</i> above) - i.e. #000000, red, #ffffff url(http://mysite.com/images/tabs.gif) - use regular css'),
    '#default_value' => $settings['denver_customptabs'],
    '#size' => 40,
    '#maxlength' => 100,
  );

  $form['denver_customptabsah'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom Tabs Hover (must select <i>Custom Tabs Color</i> or <i>Custom Tabs Background</i> above) - i.e. #000000, red, #ffffff url(http://mysite.com/images/tabs-hover.gif) - use regular css'),
    '#default_value' => $settings['denver_customptabsah'],
    '#size' => 40,
    '#maxlength' => 100,
  );

  $form['denver_customptabsaa'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom Tabs Active (must select <i>Custom Tabs Color</i> or <i>Custom Tabs Background</i> above) - i.e. #000000, red, #ffffff url(http://mysite.com/images/tabs-active.gif) - use regular css'),
    '#default_value' => $settings['denver_customptabsaa'],
    '#size' => 40,
    '#maxlength' => 100,
  );

  $settings = array_merge($defaults, $settings);
  $form['denver_plinksa'] = array(
    '#type' => 'select',
    '#title' => t('Tabs Link Colors'),
    '#default_value' => $settings['denver_plinksa'],
    '#options' => array(
      'plinksa-gray' => t('Gray/White/White (default)'),
      'plinksa-black' => t('Black/Blue/Blue'),
      'plinksa-custom' => t('Custom Tabs Link Colors'),
    ),
  );

  $form['denver_customplinksa'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom Tabs Link Color (must select <i>Custom Tabs Link Colors</i> above) - i.e. #000000, red, #ffffff - use regular css'),
    '#default_value' => $settings['denver_customplinksa'],
    '#size' => 40,
    '#maxlength' => 100,
  );

  $form['denver_customplinksah'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom Tabs Link Hover Color (must select <i>Custom Tabs Link Colors</i> above) - i.e. #000000, red, #ffffff - use regular css'),
    '#default_value' => $settings['denver_customplinksah'],
    '#size' => 40,
    '#maxlength' => 100,
  );

  $form['denver_customplinksaa'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom Tabs Link Active Color (must select <i>Custom Tabs Link Colors</i> above) - i.e. #000000, red, #ffffff - use regular css'),
    '#default_value' => $settings['denver_customplinksaa'],
    '#size' => 40,
    '#maxlength' => 100,
  );

  $settings = array_merge($defaults, $settings);
  $form['denver_searchposition'] = array(
    '#type' => 'select',
    '#title' => t('Search Position'),
    '#default_value' => $settings['denver_searchposition'],
    '#options' => array(
      'search-navright' => t('Right Navigation Area (default)'),
      'search-navcenter' => t('Center Navigation Area'),
      'search-navleft' => t('Left Navigation Area'),
      'search-headright' => t('Right Header Area'),
      'search-headcenter' => t('Center Header Area'),
      'search-headleft' => t('Left Header Area'),
    ),
  );

  $settings = array_merge($defaults, $settings);
  $form['denver_secposition'] = array(
    '#type' => 'select',
    '#title' => t('Secondary Links Position'),
    '#default_value' => $settings['denver_secposition'],
    '#options' => array(
      'secondary-center' => t('Center Secondary Links (default)'),
      'secondary-left' => t('Left Secondary Links'),
      'secondary-right' => t('Right Secondary Links'),
    ),
  );

  $settings = array_merge($defaults, $settings);
  $form['denver_secbg'] = array(
    '#type' => 'select',
    '#title' => t('Secondary Links Background'),
    '#default_value' => $settings['denver_secbg'],
    '#options' => array(
      'secbg-gray' => t('Gray (default)'),
      'secbg-blue' => t('Blue'),
      'secbg-gold' => t('Gold'),
      'secbg-trans' => t('Transparent'),
      'secbg-custom' => t('Custom Secondary Links Background'),
    ),
  );

  $form['denver_customsecbg'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom Secondary Links Background (must select <i>Custom Secondary Links Background</i> above) - i.e. #000000, red, #ffffff url(http://mysite.com/images/secondary-links-bg.gif) 0 0 repeat-x, etc. Use regular css with full URL to image'),
    '#default_value' => $settings['denver_customsecbg'],
    '#size' => 40,
    '#maxlength' => 100,
  );

  $settings = array_merge($defaults, $settings);
  $form['denver_pgbg'] = array(
    '#type' => 'select',
    '#title' => t('Page Background'),
    '#default_value' => $settings['denver_pgbg'],
    '#options' => array(
      'pgbg-lightgradient' => t('Light Gradient (default)'),
      'pgbg-tealgradient' => t('Teal Gradient'),
      'pgbg-trans' => t('Transparent'),
      'pgbg-custom' => t('Custom Page Background'),
    ),
  );

  $form['denver_custompgbg'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom Page Background (must select <i>Custom Page Background</i> above) - i.e. #000000, red, #ffffff url(http://mysite.com/images/page-bg.gif) 0 0 repeat-x, etc. Use regular css with full URL to image'),
    '#default_value' => $settings['denver_custompgbg'],
    '#size' => 40,
    '#maxlength' => 100,
  );

  $settings = array_merge($defaults, $settings);
  $form['denver_centerbg'] = array(
    '#type' => 'select',
    '#title' => t('Center Column Background'),
    '#default_value' => $settings['denver_centerbg'],
    '#options' => array(
      'centerbg-trans' => t('Transparent (default)'),
      'centerbg-white' => t('White'),
      'centerbg-lightgray' => t('Light Gray'),
      'centerbg-gray' => t('Gray'),
      'centerbg-black' => t('Black'),
      'centerbg-custom' => t('Custom Center Column Background'),
    ),
  );

  $form['denver_customcenterbg'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom Center Column Background (must select <i>Custom Center Column Background</i> above) - i.e. #000000, red, #ffffff url(http://mysite.com/images/center-column-bg.gif) 0 0 repeat-x, etc. Use regular css with full URL to image'),
    '#default_value' => $settings['denver_customcenterbg'],
    '#size' => 40,
    '#maxlength' => 100,
  );

  $settings = array_merge($defaults, $settings);
  $form['denver_pgcontentbg'] = array(
    '#type' => 'select',
    '#title' => t('Page Content Background'),
    '#default_value' => $settings['denver_pgcontentbg'],
    '#options' => array(
      'pgcontentbg-lightgradient' => t('Light Gradient (default)'),
      'pgcontentbg-tealgradient' => t('Teal Gradient'),
      'pgcontentbg-trans' => t('Transparent'),
      'pgcontentbg-custom' => t('Custom Page Content Background'),
    ),
  );

  $form['denver_custompgcontentbg'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom Page Content Background (must select <i>Custom Page Content Background</i> above) - i.e. #000000, red, #ffffff url(http://mysite.com/images/page-bg.gif) 0 0 repeat-x, etc. Use regular css with full URL to image'),
    '#default_value' => $settings['denver_custompgcontentbg'],
    '#size' => 40,
    '#maxlength' => 100,
  );

  $settings = array_merge($defaults, $settings);
  $form['denver_mainbg'] = array(
    '#type' => 'select',
    '#title' => t('Main Content Background'),
    '#default_value' => $settings['denver_mainbg'],
    '#options' => array(
      'mainbg-trans' => t('Transparent (default)'),
      'mainbg-white' => t('White'),
      'mainbg-lightgray' => t('Light Gray'),
      'mainbg-gray' => t('Gray'),
      'mainbg-black' => t('Black'),
      'mainbg-custom' => t('Custom Main Content Background'),
    ),
  );

  $form['denver_custommainbg'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom Main Content Background (must select <i>Custom Main Content Background</i> above) - i.e. #000000, red, #ffffff url(http://mysite.com/images/main-content-bg.gif) 0 0 repeat-x, etc. Use regular css with full URL to image'),
    '#default_value' => $settings['denver_custommainbg'],
    '#size' => 40,
    '#maxlength' => 100,
  );

  $settings = array_merge($defaults, $settings);
  $form['denver_nodebg'] = array(
    '#type' => 'select',
    '#title' => t('Node Background'),
    '#default_value' => $settings['denver_nodebg'],
    '#options' => array(
      'nodebg-trans' => t('Transparent (default)'),
      'nodebg-white' => t('White'),
      'nodebg-lightgray' => t('Light Gray'),
      'nodebg-gray' => t('Gray'),
      'nodebg-black' => t('Black'),
      'nodebg-custom' => t('Custom Node Background'),
    ),
  );

  $form['denver_customnodebg'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom Node Background (must select <i>Custom Node Background</i> above) - i.e. #000000, red, #ffffff url(http://mysite.com/images/node-bg.gif) 0 0 repeat-x, etc. Use regular css with full URL to image'),
    '#default_value' => $settings['denver_customnodebg'],
    '#size' => 40,
    '#maxlength' => 100,
  );

  $settings = array_merge($defaults, $settings);
  $form['denver_nodecontentbg'] = array(
    '#type' => 'select',
    '#title' => t('Node Content Background'),
    '#default_value' => $settings['denver_nodecontentbg'],
    '#options' => array(
      'nodecontentbg-trans' => t('Transparent (default)'),
      'nodecontentbg-white' => t('White'),
      'nodecontentbg-lightgray' => t('Light Gray'),
      'nodecontentbg-gray' => t('Gray'),
      'nodecontentbg-black' => t('Black'),
      'nodecontentbg-custom' => t('Custom Node Content Background'),
    ),
  );

  $form['denver_customnodecontentbg'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom Node Content Background (must select <i>Custom Node Content Background</i> above) - i.e. #000000, red, #ffffff url(http://mysite.com/images/node-content-bg.gif) 0 0 repeat-x, etc. Use regular css with full URL to image'),
    '#default_value' => $settings['denver_customnodecontentbg'],
    '#size' => 40,
    '#maxlength' => 100,
  );

  $settings = array_merge($defaults, $settings);
  $form['denver_leftsidebarwidth'] = array(
    '#type' => 'select',
    '#title' => t('Left Sidebar Width'),
    '#default_value' => $settings['denver_leftsidebarwidth'],
    '#options' => array(
      'leftsidebarwidth-200' => t('200px (default)'),
      'leftsidebarwidth-300' => t('300px'),
      'leftsidebarwidth-150' => t('150px'),
      'leftsidebarwidth-custom' => t('Custom Left Sidebar Width'),
    ),
  );

  $form['denver_customleftsidebarwidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom Left Sidebar Width in pixels (must select Custom Left Sidebar Width above) - i.e. 180px, etc'),
    '#default_value' => $settings['denver_customleftsidebarwidth'],
    '#size' => 6,
    '#maxlength' => 6,
  );

  $settings = array_merge($defaults, $settings);
  $form['denver_rightsidebarwidth'] = array(
    '#type' => 'select',
    '#title' => t('Right Sidebar Width'),
    '#default_value' => $settings['denver_rightsidebarwidth'],
    '#options' => array(
      'rightsidebarwidth-200' => t('200px (default)'),
      'rightsidebarwidth-300' => t('300px'),
      'rightsidebarwidth-150' => t('150px'),
      'rightsidebarwidth-custom' => t('Custom Right Sidebar Width'),
    ),
  );

  $form['denver_customrightsidebarwidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom Right Sidebar Width in pixels or em (must select Custom Right Sidebar Width above) - i.e. 180px, 20em, etc'),
    '#default_value' => $settings['denver_customrightsidebarwidth'],
    '#size' => 6,
    '#maxlength' => 6,
  );

  $settings = array_merge($defaults, $settings);
  $form['denver_leftbg'] = array(
    '#type' => 'select',
    '#title' => t('Left Column Background'),
    '#default_value' => $settings['denver_leftbg'],
    '#options' => array(
      'leftbg-trans' => t('Transparent (default)'),
      'leftbg-white' => t('White'),
      'leftbg-lightgray' => t('Light Gray'),
      'leftbg-gray' => t('Gray'),
      'leftbg-black' => t('Black'),
      'leftbg-custom' => t('Custom Left Column Background'),
    ),
  );

  $form['denver_customleftbg'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom Left Column Background (must select <i>Custom Left Column Background</i> above) - i.e. #000000, red, #ffffff url(http://mysite.com/images/left-column-bg.gif) 0 0 repeat-x, etc. Use regular css with full URL to image'),
    '#default_value' => $settings['denver_customleftbg'],
    '#size' => 40,
    '#maxlength' => 100,
  );

  $settings = array_merge($defaults, $settings);
  $form['denver_blocklbg'] = array(
    '#type' => 'select',
    '#title' => t('Left Blocks Background'),
    '#default_value' => $settings['denver_blocklbg'],
    '#options' => array(
      'blocklbg-trans' => t('Transparent (default)'),
      'blocklbg-white' => t('White'),
      'blocklbg-lightgray' => t('Light Gray'),
      'blocklbg-gray' => t('Gray'),
      'blocklbg-black' => t('Black'),
      'blocklbg-custom' => t('Custom Left Blocks Background'),
    ),
  );

  $form['denver_customblocklbg'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom Left Blocks Background (must select <i>Custom Left Blocks Background</i> above) - i.e. #000000, red, #ffffff url(http://mysite.com/images/left-blocks-bg.gif) 0 0 repeat-x, etc. Use regular css with full URL to image'),
    '#default_value' => $settings['denver_customblocklbg'],
    '#size' => 40,
    '#maxlength' => 100,
  );

  $settings = array_merge($defaults, $settings);
  $form['denver_blockltitle'] = array(
    '#type' => 'select',
    '#title' => t('Left Blocks Title Background'),
    '#default_value' => $settings['denver_blockltitle'],
    '#options' => array(
      'blockltitle-lightblue' => t('Light Blue (default)'),
      'blockltitle-blue' => t('Blue'),
      'blockltitle-gold' => t('Gold'),
      'blockltitle-gray' => t('Gray'),
      'blockltitle-red' => t('Red'),
      'blockltitle-mauve' => t('Mauve'),
      'blockltitle-trans' => t('Transparent'),
      'blockltitle-custom' => t('Custom Left Blocks Title Background'),
    ),
  );

  $form['denver_customblockltitle'] = array(
    '#type' => 'textfield',
    '#title' => t('Left Blocks Title Background (must select <i>Custom Left Blocks Title Background</i> above) - i.e. #000000, navy, #ffffff url(http://mysite.com/left-blocks-title-bg.gif) 0 0 repeat-x, etc. Use regular css with full URL to image'),
    '#default_value' => $settings['denver_customblockltitle'],
    '#size' => 40,
    '#maxlength' => 100,
  );

  $settings = array_merge($defaults, $settings);
  $form['denver_blocklcontent'] = array(
    '#type' => 'select',
    '#title' => t('Left Blocks Content Background'),
    '#default_value' => $settings['denver_blocklcontent'],
    '#options' => array(
      'blocklcontent-lightgray' => t('Light Gray (default)'),
      'blocklcontent-white' => t('White'),
      'blocklcontent-gray' => t('Gray'),
      'blocklcontent-black' => t('Black'),
      'blocklcontent-trans' => t('Transparent'),
      'blocklcontent-custom' => t('Custom Left Blocks Content Background'),
    ),
  );

  $form['denver_customblocklcontent'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom Left Blocks Content Background (must select <i>Custom Left Blocks Content Background</i> above) - i.e. #000000, red, #ffffff url(http://mysite.com/images/left-blocks-content-bg.gif) 0 0 repeat-x, etc. Use regular css with full URL to image'),
    '#default_value' => $settings['denver_customblocklcontent'],
    '#size' => 40,
    '#maxlength' => 100,
  );

  $settings = array_merge($defaults, $settings);
  $form['denver_rightbg'] = array(
    '#type' => 'select',
    '#title' => t('Right Column Background'),
    '#default_value' => $settings['denver_rightbg'],
    '#options' => array(
      'rightbg-trans' => t('Transparent (default)'),
      'rightbg-white' => t('White'),
      'rightbg-lightgray' => t('Light Gray'),
      'rightbg-gray' => t('Gray'),
      'rightbg-black' => t('Black'),
      'rightbg-custom' => t('Custom Right Column Background'),
    ),
  );

  $form['denver_customrightbg'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom Right Column Background (must select <i>Custom Right Column Background</i> above) - i.e. #000000, red, #ffffff url(http://mysite.com/images/right-column-bg.gif) 0 0 repeat-x, etc. Use regular css with full URL to image'),
    '#default_value' => $settings['denver_customrightbg'],
    '#size' => 40,
    '#maxlength' => 100,
  );  
  
  $settings = array_merge($defaults, $settings);
  $form['denver_blockrbg'] = array(
    '#type' => 'select',
    '#title' => t('Right Blocks Background'),
    '#default_value' => $settings['denver_blockrbg'],
    '#options' => array(
      'blockrbg-trans' => t('Transparent (default)'),
      'blockrbg-white' => t('White'),
      'blockrbg-lightgray' => t('Light Gray'),
      'blockrbg-gray' => t('Gray'),
      'blockrbg-black' => t('Black'),
      'blockrbg-custom' => t('Custom Right Blocks Background'),
    ),
  );

  $form['denver_customblockrbg'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom Right Blocks Background (must select <i>Custom Right Blocks Background</i> above) - i.e. #000000, red, #ffffff url(http://mysite.com/images/right-blocks-bg.gif) 0 0 repeat-x, etc. Use regular css with full URL to image'),
    '#default_value' => $settings['denver_customblockrbg'],
    '#size' => 40,
    '#maxlength' => 100,
  );
  
  $settings = array_merge($defaults, $settings);
  $form['denver_blockrtitle'] = array(
    '#type' => 'select',
    '#title' => t('Right Blocks Title Background'),
    '#default_value' => $settings['denver_blockrtitle'],
    '#options' => array(
      'blockrtitle-lightblue' => t('Light Blue (default)'),
      'blockrtitle-blue' => t('Blue'),
      'blockrtitle-gold' => t('Gold'),
      'blockrtitle-gray' => t('Gray'),
      'blockrtitle-red' => t('Red'),
      'blockrtitle-mauve' => t('Mauve'),
      'blockrtitle-trans' => t('Transparent'),
      'blockrtitle-custom' => t('Custom Right Blocks Title Background'),
    ),
  );

  $form['denver_customblockrtitle'] = array(
    '#type' => 'textfield',
    '#title' => t('Right Blocks Title Background - i.e. #000000, navy, #ffffff url(http://mysite.com/right-blocks-title-bg.gif) 0 0 repeat-x, etc. Use regular css with full URL to image'),
    '#default_value' => $settings['denver_customblockrtitle'],
    '#size' => 40,
    '#maxlength' => 100,
  );
  
  $settings = array_merge($defaults, $settings);
  $form['denver_blockrcontent'] = array(
    '#type' => 'select',
    '#title' => t('Right Blocks Content Background'),
    '#default_value' => $settings['denver_blockrcontent'],
    '#options' => array(
      'blockrcontent-lightgray' => t('Light Gray (default)'),
      'blockrcontent-white' => t('White'),
      'blockrcontent-gray' => t('Gray'),
      'blockrcontent-black' => t('Black'),
      'blockrcontent-trans' => t('Transparent'),
      'blockrcontent-custom' => t('Custom Right Blocks Content Background'),
    ),
  );

  $form['denver_customblockrcontent'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom Right Blocks Content Background (must select <i>Custom Right Blocks Content Background</i> above) - i.e. #000000, red, #ffffff url(http://mysite.com/images/right-blocks-content-bg.gif) 0 0 repeat-x, etc. Use regular css with full URL to image'),
    '#default_value' => $settings['denver_customblockrcontent'],
    '#size' => 40,
    '#maxlength' => 100,
  );
  
  $settings = array_merge($defaults, $settings);
  $form['denver_footerbg'] = array(
    '#type' => 'select',
    '#title' => t('Footer Background'),
    '#default_value' => $settings['denver_footerbg'],
    '#options' => array(
      'footerbg-darkgray' => t('Dark Gray (default)'),
      'footerbg-white' => t('White'),
      'footerbg-lightgray' => t('Light Gray'),
      'footerbg-gray' => t('Gray'),
      'footerbg-black' => t('Black'),
      'footerbg-trans' => t('Transparent'),
      'footerbg-custom' => t('Custom Footer Background'),
    ),
  );

  $form['denver_customfooterbg'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom Footer Background (must select <i>Custom Footer Background</i> above) - i.e. #000000, red, #ffffff url(http://mysite.com/images/footer-bg.gif) 0 0 repeat-x, etc. Use regular css with full URL to image'),
    '#default_value' => $settings['denver_customfooterbg'],
    '#size' => 40,
    '#maxlength' => 100,
  );  
  
  $form['css']['denver_cssfile'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use CSS File'),
    '#default_value' => $settings['denver_cssfile'],
  );

  $form['css']['denver_customcssfile'] = array(
    '#type' => 'textfield',
    '#title' => t('CSS File Location - i.e. http://mysite.com/my-stylesheet.css - Use full URL to file'),
    '#default_value' => $settings['denver_customcssfile'],
    '#size' => 40,
    '#maxlength' => 100,
  );

  $form['css']['denver_csstype'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Inline CSS'),
    '#default_value' => $settings['denver_csstype'],
  );

  $form['css']['denver_customcsstype'] = array(
    '#type' => 'textarea',
    '#title' => t('Inline CSS - Type in some regular css as in a .css file with full URLs to any images. Use this to add or override styles for this theme. This also overrides the above Custom CSS File'),
    '#default_value' => $settings['denver_customcsstype'],
    '#cols' => 80,
    '#rows' => 10,
    '#maxlength' => 10000,
  );

  $form['js']['denver_jsfile'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Javascript File'),
    '#default_value' => $settings['denver_jsfile'],
  );

  $form['js']['denver_customjsfile'] = array(
    '#type' => 'textfield',
    '#title' => t('Javascript File Location - i.e. http://mysite.com/my-stylesheet.js - Use full URL to file'),
    '#default_value' => $settings['denver_customjsfile'],
    '#size' => 40,
    '#maxlength' => 100,
  );

  $form['js']['denver_jstype'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Inline Javascript'),
    '#default_value' => $settings['denver_jstype'],
  );

  $form['js']['denver_customjstype'] = array(
    '#type' => 'textarea',
    '#title' => t('Inline Javascript - Type in some regular javascript as in a .js file with full URLs to any images. Use this to add or override styles for this theme. This is inserted into the <head> of page.tpl.php'),
    '#default_value' => $settings['denver_customjstype'],
    '#cols' => 80,
    '#rows' => 10,
    '#maxlength' => 10000,
  );

  $form['denver_breadcrumb'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Breadcrumbs'),
    '#default_value' => $settings['denver_breadcrumb'],
  );

  $form['denver_iepngfix'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use IE PNG Fix'),
    '#default_value' => $settings['denver_iepngfix'],
  );

  $form['denver_themelogo'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Themed Logo'),
    '#default_value' => $settings['denver_themelogo'],
  );

  $form['denver_suckerfish'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Suckerfish Menus'),
    '#default_value' => $settings['denver_suckerfish'],
  );

  $settings = array_merge($defaults, $settings);
  $form['denver_suckerfishcolor'] = array(
    '#type' => 'select',
    '#title' => t('Suckerfish Menu Color'),
    '#default_value' => $settings['denver_suckerfishcolor'],
    '#options' => array(
      'blue' => t('blue (default)'),
      'green' => t('Green'),
      'brown' => t('Brown'),
      'red' => t('Red'),
    ),
  );

  return $form;
}

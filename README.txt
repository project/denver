The center column content renders first, for the best search engine optimization. Page layout is based on the Jello Mold technique. Validates both Valid XHTML 1.0 Strict and W3C CSS. In addition, this theme uses the Theme Settings API Module (http://drupal.org/project/themesettingsapi - which you must install to get the extra theme settings below) extensively to provide 35 custom theme settings.

This theme's developer, colorado (http://drupal.org/user/18028) can be hired to customize and add extra theme settings, features, graphics and color sets to this theme or any other theme.

Current theme settings include:

* 16 Collapsible Custom Block Regions
* Tabbed Primary Links with Selectable Color Sets
* Primary Links Background and Position
* Suckerfish drop-down menu
* Search Position
* Extra CSS Additions and Overrides (add CSS Files, as well as manual CSS input)
* Extra Javascript Additions (add Javascript Files, as well as manual Javascript input)
* Page Width
* Body Padding
* Body Background
* Header Background
* Page Background
* Secondary Links Background and Position
* Content Area Background
* Left Column Background
* Right Column Background
* Center Column Background
* Left Block Header and Content Backgrounds
* Right Block Header and Content Backgrounds
* Footer Background

IMPORTANT:

When using this theme, to prevent rendering errors in IE you must enable the CSS Aggregator in admin/settings/performance.

You can find that setting at the bottom of the admin menu under Administer -> Site Configuration -> Performance (at the bottom of the page) -> Aggregate and compress CSS files:

Set it to enabled to make all of your production drupal sites run faster, especially if you are loading a lot of stylesheets, and to prevent IE rendering errors.

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
  <!--[if IE]>
  <style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() ?>/hacks/ie.css";</style>
  <![endif]-->
  <!--[if IE 5]>
  <style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() ?>/hacks/ie5.css";</style>
  <![endif]-->
  <!--[if IE 6]>
  <style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() ?>/hacks/ie6.css";</style>
  <![endif]-->

<?php 
$style = theme_get_setting('denver_style');
if (!$style)
{
$style = '';
}
?>

<?php 
$font = theme_get_setting('denver_font');
if (!$font)
{
$font = '';
}
?>
    
<?php 
$width = theme_get_setting('denver_width');
if (!$width)
{
$width = '';
}
?>
    
<?php 
$padding = theme_get_setting('denver_padding');
if (!$padding)
{
$padding = '';
}
?>
  
<?php 
$bodybg = theme_get_setting('denver_bodybg');
if (!$bodybg)
{
   $bodybg = '';
}
?>

<?php 
$pgheader = theme_get_setting('denver_pgheader');
if (!$pgheader)
{
   $pgheader = '';
}
 ?>
 
<?php 
$pgheaderh = theme_get_setting('denver_pgheaderh');
if (!$pgheaderh)
{
   $pgheaderh = '';
}
 ?> 

<?php 
$logohalign = theme_get_setting('denver_logohalign');
if (!$logohalign)
{
   $logohalign = '';
}
 ?>

<?php 
$navbg = theme_get_setting('denver_navbg');
if (!$navbg)
{
   $navbg = '';
}
 ?>

<?php 
$ptabsposition = theme_get_setting('denver_ptabsposition');
if (!$ptabsposition)
{
   $ptabsposition = '';
}
 ?>

<?php 
$ptabs = theme_get_setting('denver_ptabs');
if (!$ptabs)
{
   $ptabs = '';
}
 ?>

<?php 
$plinksa = theme_get_setting('denver_plinksa');
if (!$plinksa)
{
   $plinksa = '';
}
 ?>

<?php 
$searchposition = theme_get_setting('denver_searchposition');
if (!$searchposition)
{
   $searchposition = '';
}
 ?>

<?php 
$secposition = theme_get_setting('denver_secposition');
if (!$secposition)
{
   $secposition = '';
}
 ?>

<?php 
$secbg = theme_get_setting('denver_secbg');
if (!$secbg)
{
   $secbg = '';
}
 ?>

<?php 
$pgbg = theme_get_setting('denver_pgbg');
if (!$pgbg)
{
   $pgbg = '';
}
 ?>

<?php 
$pgcontentbg = theme_get_setting('denver_pgcontentbg');
if (!$pgcontentbg)
{
   $pgcontentbg = '';
}
 ?>

<?php 
$leftsidebarwidth = theme_get_setting('denver_leftsidebarwidth');
if (!$leftsidebarwidth)
{
   $leftsidebarwidth = '';
}
 ?>
 
<?php 
$rightsidebarwidth = theme_get_setting('denver_rightsidebarwidth');
if (!$rightsidebarwidth)
{
   $rightsidebarwidth = '';
}
 ?> 

<?php 
$leftbg = theme_get_setting('denver_leftbg');
if (!$leftbg)
{
   $leftbg = '';
}
 ?>

<?php 
$blocklbg = theme_get_setting('denver_blocklbg');
if (!$blocklbg)
{
   $blocklbg = '';
}
 ?>

<?php 
$blockltitle = theme_get_setting('denver_blockltitle');
if (!$blockltitle)
{
   $blockltitle = '';
}
 ?>

<?php 
$blocklcontent = theme_get_setting('denver_blocklcontent');
if (!$blocklcontent)
{
   $blocklcontent = '';
}
 ?>

<?php 
$centerbg = theme_get_setting('denver_centerbg');
if (!$centerbg)
{
   $centerbg = '';
}
 ?>
 
<?php 
$mainbg = theme_get_setting('denver_mainbg');
if (!$mainbg)
{
   $mainbg = '';
}
 ?>

<?php 
$nodebg = theme_get_setting('denver_nodebg');
if (!$nodebg)
{
   $nodebg = '';
}
 ?>
 
<?php 
$nodecontentbg = theme_get_setting('denver_nodecontentbg');
if (!$nodecontentbg)
{
   $nodecontentbg = '';
}
 ?>
 
<?php 
$rightbg = theme_get_setting('denver_rightbg');
if (!$rightbg)
{
   $rightbg = '';
}
?>

<?php 
$blockrbg = theme_get_setting('denver_blockrbg');
if (!$blockrbg)
{
   $blockrbg = '';
}
?>
 
<?php 
$blockrtitle = theme_get_setting('denver_blockrtitle');
if (!$blockrtitle)
{
   $blockrtitle = '';
}
?>

<?php 
$blockrcontent = theme_get_setting('denver_blockrcontent');
if (!$blockrcontent)
{
   $blockrcontent = '';
}
?>

<?php 
$footerbg = theme_get_setting('denver_footerbg');
if (!$footerbg)
{
   $footerbg = '';
}
?>

<style type="text/css">

   <?php if ($bodybg=='bodybg-custom-color'):?>
      	body {background: <?php print theme_get_setting('denver_custombodybg') ?>;}
   <?php endif; ?>

   <?php if ($bodybg=='bodybg-custom'):?>
      	body {background: <?php print theme_get_setting('denver_custombodybg') ?>;}
   <?php endif; ?>

   <?php if ($font=='font-custom'):?>
	body {font-family : <?php print theme_get_setting('denver_customfont') ?>;}
   <?php endif; ?>
   
   <?php if ($width=='width-custom'):?>
	#sizer {max-width: <?php print theme_get_setting('denver_customwidth') ?>;}
	#page {width: <?php print theme_get_setting('denver_customwidth') ?>;}
   <?php endif; ?>

   <?php if ($padding=='padding-custom'):?>
      	#page {padding: <?php print theme_get_setting('denver_custompadding') ?>;}
   <?php endif; ?>

   <?php if ($pgheader=='pgheader-custom-color'):?>
      	div#masthead {background: <?php print theme_get_setting('denver_custompgheader') ?>;}
   <?php endif; ?>

   <?php if ($pgheader=='pgheader-custom'):?>
      	div#masthead {background: <?php print theme_get_setting('denver_custompgheader') ?>;}
   <?php endif; ?>
  
   <?php if ($pgheaderh=='pgheaderh-custom'):?>
      	div#masthead {height: <?php print theme_get_setting('denver_custompgheaderh') ?>;}
   <?php endif; ?>   

   <?php if ($navbg=='navbg-custom'):?>
      	#navigation {background: <?php print theme_get_setting('denver_customnavbg') ?>;}
   <?php endif; ?>
   
   <?php if ($navbg=='navbg-customgradient'):?>
      	#navigation {background: <?php print theme_get_setting('denver_customnavbg') ?> url(<?php print base_path() . path_to_theme() ?>/images/menu-bg-custom-gradient.png) 0 0 repeat-x;}
   <?php endif; ?>   

   <?php if ($ptabs=='ptabs-custom-color'):?>
        ul#primary-links li, ul#primary-links li:hover, ul#primary-links li.active {background-color:<?php print theme_get_setting('denver_customptabs') ?>;}
        ul#primary-links li:hover {background-color:<?php print theme_get_setting('denver_customptabsah') ?>;}
        ul#primary-links li.active {background-color:<?php print theme_get_setting('denver_customptabsaa') ?>;}
   <?php endif; ?>

   <?php if ($ptabs=='ptabs-custom'):?>
        ul#primary-links li, ul#primary-links li:hover, ul#primary-links li.active {background:<?php print theme_get_setting('denver_customptabs') ?>;}
        ul#primary-links li:hover {background:<?php print theme_get_setting('denver_customptabsah') ?>;}
        ul#primary-links li.active {background:<?php print theme_get_setting('denver_customptabsaa') ?>;}
   <?php endif; ?>

   <?php if ($plinksa=='plinksa-custom'):?>
        ul#primary-links li a, ul#primary-links li:hover a, ul#primary-links li.active a {color:<?php print theme_get_setting('denver_customplinksa') ?>;}
        ul#primary-links li:hover a {color:<?php print theme_get_setting('denver_customplinksah') ?>;}
        ul#primary-links li.active a {color:<?php print theme_get_setting('denver_customplinksaa') ?>;}
   <?php endif; ?>

   <?php if ($secbg=='secbg-custom'):?>
      	#secondary {background : <?php print theme_get_setting('denver_customsecbg') ?>;}
   <?php endif; ?>
   
   <?php if ($pgbg=='pgbg-custom'):?>
      	#page-wrapper {background : <?php print theme_get_setting('denver_custompgbg') ?>;}
   <?php endif; ?>

   <?php if ($centerbg=='centerbg-custom'):?>
      	#center {background : <?php print theme_get_setting('denver_customcenterbg') ?>;}
   <?php endif; ?>

   <?php if ($pgcontentbg=='pgcontentbg-custom'):?>
      	#content {background : <?php print theme_get_setting('denver_custompgcontentbg') ?>;}
   <?php endif; ?>

   <?php if ($mainbg=='mainbg-custom'):?>
      	#main {background : <?php print theme_get_setting('denver_custommainbg') ?>;}
   <?php endif; ?>
   
   <?php if ($nodebg=='nodebg-custom'):?>
      	.node {background : <?php print theme_get_setting('denver_customnodebg') ?>;}
   <?php endif; ?>
   
   <?php if ($nodecontentbg=='nodecontentbg-custom'):?>
      	.node .content {background : <?php print theme_get_setting('denver_customnodecontentbg') ?>;}
   <?php endif; ?>
   
   <?php if ($leftsidebarwidth=='leftsidebarwidth-custom'):?>
	#wrapper1 .outer {margin-left: <?php print theme_get_setting('denver_customleftsidebarwidth') ?>;}
	#wrapper1 #wrapper2 .outer {margin-left: <?php print theme_get_setting('denver_customleftsidebarwidth') ?>;}
	#sidebar-left {width: <?php print theme_get_setting('denver_customleftsidebarwidth') ?>; margin-left: -<?php print theme_get_setting('denver_customleftsidebarwidth') ?>;}
   <?php endif; ?>
   
   <?php if ($rightsidebarwidth=='rightsidebarwidth-custom'):?>
	#wrapper2 .outer {margin-right: <?php print theme_get_setting('denver_customrightsidebarwidth') ?>;}
	#wrapper1 #wrapper2 .outer {margin-right: <?php print theme_get_setting('denver_customrightsidebarwidth') ?>;}
	#sidebar-right {width: <?php print theme_get_setting('denver_customrightsidebarwidth') ?>; margin-right: -<?php print theme_get_setting('denver_customrightsidebarwidth') ?>; margin-left: 0;}
   <?php endif; ?>   
 
   <?php if ($leftbg=='leftbg-custom'):?>
      	#sidebar-left {background : <?php print theme_get_setting('denver_customleftbg') ?>;}
   <?php endif; ?>

   <?php if ($blocklbg=='blocklbg-custom'):?>
      	#leftsidebar .block {background : <?php print theme_get_setting('denver_customblocklbg') ?>;}
   <?php endif; ?>

   <?php if ($blockltitle=='blockltitle-custom'):?>
      	#leftsidebar .titlewrap {background : <?php print theme_get_setting('denver_customblockltitle') ?>;}
   <?php endif; ?>

   <?php if ($blocklcontent=='blocklcontent-custom'):?>
      	#leftsidebar .content {background : <?php print theme_get_setting('denver_customblocklcontent') ?>;}
   <?php endif; ?>

   <?php if ($rightbg=='rightbg-custom'):?>
      	#sidebar-right {background : <?php print theme_get_setting('denver_customrightbg') ?>;}
   <?php endif; ?>

   <?php if ($blockrbg=='blockrbg-custom'):?>
      	#rightsidebar .block {background : <?php print theme_get_setting('denver_customblockrbg') ?>;}
   <?php endif; ?>
   
   <?php if ($blockrtitle=='blockrtitle-custom'):?>
      	#rightsidebar .titlewrap {background : <?php print theme_get_setting('denver_customblockrtitle') ?>;}
   <?php endif; ?>
   
   <?php if ($blockrcontent=='blockrcontent-custom'):?>
      	#rightsidebar .content {background : <?php print theme_get_setting('denver_customblockrcontent') ?>;}
   <?php endif; ?>

   <?php if ($footerbg=='footerbg-custom'):?>
      	div#footer-wrapper {background : <?php print theme_get_setting('denver_customfooterbg') ?>;}
   <?php endif; ?>

   <?php if (theme_get_setting('denver_csstype')) { ?>
      	<?php print theme_get_setting('denver_customcsstype') ?>
   <?php } else { ?>
   <?php }  ?>

</style>

   <?php if (theme_get_setting('denver_cssfile')) { ?>
     <style type="text/css" media="all">@import "<?php print theme_get_setting('denver_customcssfile') ?>";</style>
   <?php } else { ?>
   <?php }  ?>

   <?php if (theme_get_setting('denver_jsfile')) { ?>
      <script type="text/javascript" src="<?php print theme_get_setting('denver_customjsfile') ?>"></script>
   <?php } else { ?>
   <?php }  ?>

   <?php if (theme_get_setting('denver_jstype')) { ?>
     <script type="text/javascript"><?php print theme_get_setting('denver_customjstype') ?></script>
   <?php } else { ?>
   <?php }  ?>

   <?php if (theme_get_setting('denver_iepngfix')) { ?>
   <!--[if lte IE 6]>
   <script type="text/javascript"> 
    $(document).ready(function(){ 
        $(document).pngFix(); 
    }); 
   </script> 
   <![endif]-->
   <?php } ?>
   
   <?php /* custom stuff for block display/closure */ ?>
   <script type="text/javascript" src="<?php print $GLOBALS['base_url']."/"; print $directory;
   ?>/js/pickstyle.js"></script>

</head>

<body>

<div id="sizer">
<div id="expander">
<div id="wrapper">
<div id="page">

<?php if ($page_top):?><div id="page-top-region"><?php print $page_top; ?></div><?php endif; ?>

<div class="header">

<?php if ($pgheader !== 'pgheader-none'){ ?>

<div id="masthead" class="clear-block">
    <div class="header-left">
    <div class="header-right">
        <table id="header" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td id="logo" >
              <?php if ($header_right):?><div id="header-right-region" ><?php print $header_right ?></div><?php endif; ?>
              <?php if ($logo) { ?><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a><?php } ?>
              <?php if ($site_name) { ?><h1 class='site-name'><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
              <?php if ($site_slogan) { ?><div class='site-slogan'><?php print $site_slogan ?></div><?php } ?>
            </td>
          </tr>
          <?php if ($header):?><tr><td><div id="header-region"><?php print $header ?></div></td></tr><?php endif; ?>
        </table>
        <table id="headnavbot" border="0" cellpadding="0" cellspacing="0"><tr><td>
          <?php if ($ptabsposition == 'ptabs-headleft'):?><td class="menu <?php if ($primary_links) { print "withprimary"; } if ($secondary_links) { print " withsecondary"; } ?> ">
        	    <?php if (isset($primary_links)) : ?><?php print theme('linksnew', $primary_links, array('id' => 'primary-links')) ?><?php endif; ?></td>
          <?php endif; ?>
          <?php if ($searchposition == 'search-headleft'):?><td class="searchwrap"><?php print $search_box; ?></td>
          <?php endif; ?>
          <?php if ($searchposition == 'search-headright'):?><td class="searchwrap"><?php print $search_box; ?></td>
          <?php endif; ?>
          <?php if ($searchposition == 'search-headcenter'):?><td class="searchwrap"><?php print $search_box; ?></td>
          <?php endif; ?>
          <?php if ($ptabsposition == 'ptabs-headright'):?><td class="menu <?php if ($primary_links) { print "withprimary"; } if ($secondary_links) { print " withsecondary"; } ?> ">
        	    <?php if (isset($primary_links)) : ?><?php print theme('linksnew', $primary_links, array('id' => 'primary-links')) ?><?php endif; ?></td>
          <?php endif; ?>
        </td></tr></table>
    </div>
    </div>
</div>

<?php } else { ?>
<?php }  ?>

<?php if ($navbg !== 'navbg-none'){ ?>

   <div id="navigation"><table width="100%" border="0" cellpadding="0" cellspacing="0"><tr>

          <?php if ($ptabsposition == 'ptabs-navleft'):?><td class="menu <?php if ($primary_links) { print "withprimary"; } if ($secondary_links) { print " withsecondary"; } ?> ">
        	    <?php if (isset($primary_links)) : ?><?php print theme('linksnew', $primary_links, array('id' => 'primary-links')) ?><?php endif; ?></td><?php endif; ?>

          <?php if ($searchposition == 'search-navleft'):?><td class="searchwrap"><?php print $search_box; ?></td><?php endif; ?>

          <?php if ($ptabsposition == 'ptabs-navright'):?><td class="menu <?php if ($primary_links) { print "withprimary"; } if ($secondary_links) { print " withsecondary"; } ?> ">
        	    <?php if (isset($primary_links)) : ?><?php print theme('linksnew', $primary_links, array('id' => 'primary-links')) ?><?php endif; ?></td><?php endif; ?>

          <?php if ($searchposition == 'search-navcenter'):?><td class="searchwrap"><?php print $search_box; ?></td> <?php endif; ?>

          <?php if ($searchposition == 'search-navright'):?><td class="searchwrap"><?php print $search_box; ?></td><?php endif; ?>

   </tr></table></div>

<?php } else { ?>
<?php }  ?>

      <?php if ($secondary_links): ?>
        <table id="secondary" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td>
              <?php print theme('menu_links', $secondary_links); ?>
            </td>
          </tr>
        </table>
      <?php endif; ?>
      
      <?php if ($suckerfish_menu): ?>
        <div id="suckerfishmenu" class="clear-block">
          <?php print $suckerfish_menu; ?>
        </div>
      <?php endif; ?>


      <?php if ($sub_nav): ?>
        <table id="sub-nav-region" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td>
              <div id="sub-nav">
                <?php print $sub_nav; ?>
              </div>
            </td>
          </tr>
        </table>
      <?php endif; ?>
      
</div>

<div id="page-wrapper">

<?php if ($sidebar_left):?>
<div id="wrapper1">
<?php endif; ?>

<?php if ($sidebar_right):?>
<div id="wrapper2">
<?php endif; ?>

<div class="outer">
<div class="float-wrap">
<div class="center-wrap">
<div class="centerbox">
<div class="textpadder">

  <div id="center">
    <div id="content">
         <?php if ($node_top):?><div id="node-top-region"><?php print $node_top; ?></div><?php endif; ?>
         <?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
      <div id="main">
        <?php if (theme_get_setting('denver_breadcrumb')): ?>
          <?php if ($breadcrumb): ?><div id="breadcrumb"><?php print $breadcrumb; ?></div><?php endif; ?>
        <?php endif; ?>

        <?php if ($content_top):?><div id="content-top"><?php print $content_top; ?></div><?php endif; ?>

	<?php
         $section1count = 0;
         $user1count = 0;
         $user2count = 0;
         $user3count = 0;

         if ($user1)
         {
            $section1count++;
            $user1count++;
         }

         if ($user2)
         {
            $section1count++;
            $user2count++;
         }
         
         if ($user3)
         {
            $section1count++;
            $user3count++;
         }
      ?>

      <?php if ($section1count): ?>
         <?php $section1width = 'width' . floor(99 / $section1count); ?>
         <?php $block2div = ($user1count and ($user2count or $user3count)) ? " divider" : ""; ?>
         <?php $block3div = ($user3count and ($user1count or $user2count)) ? " divider" : ""; ?>
	   <div class="clr" id="section1">

         <table class="sections" cellspacing="0" cellpadding="0">
            <tr valign="top">

            <?php if ($user1): ?>
               <td class="section <?php echo $section1width ?>">
                  <?php print $user1; ?>
               </td>
            <?php endif; ?>  

            <?php if ($user2): ?>
               <td class="section <?php echo $section1width . $block2div; ?>">
                  <?php print $user2; ?>
               </td>
            <?php endif; ?>  

            <?php if ($user3): ?>
               <td class="section <?php echo $section1width . $block3div; ?>">
                  <?php print $user3; ?>
               </td>
            <?php endif; ?>  

            </tr>
         </table>

         </div>
      <?php endif; ?>

        <h1 class="title"><?php print $title ?></h1>
        <div class="tabs"><?php print $tabs ?></div>
        <?php print $help ?>
        <?php print $messages ?>
        <?php print $content; ?>
        <?php print $feed_icons; ?>

	<?php
         $section2count = 0;
         $user4count = 0;
         $user5count = 0;
         $user6count = 0;

         if ($user4)
         {
            $section2count++;
            $user4count++;
         }

         if ($user5)
         {
            $section2count++;
            $user5count++;
         }
         
         if ($user6)
         {
            $section2count++;
            $user6count++;
         }
      ?>

      <?php if ($section2count): ?>
         <?php $section2width = 'width' . floor(99 / $section2count); ?>
         <?php $block2div = ($user4count and ($user5count or $user6count)) ? " divider" : ""; ?>
         <?php $block3div = ($user6count and ($user4count or $user5count)) ? " divider" : ""; ?>
	   <div class="clr" id="section2">

       <table class="sections" cellspacing="0" cellpadding="0">
            <tr valign="top">

            <?php if ($user4): ?>
               <td class="section <?php echo $section2width ?>">
                  <?php print $user4; ?>
               </td>
            <?php endif; ?>  

            <?php if ($user5): ?>
               <td class="section <?php echo $section2width . $block2div; ?>">
                  <?php print $user5; ?>
               </td>
            <?php endif; ?>  

            <?php if ($user6): ?>
               <td class="section <?php echo $section2width . $block3div; ?>">
                  <?php print $user6; ?>
               </td>
            <?php endif; ?>  

            </tr>
         </table>

         </div>
      <?php endif; ?>

        <?php if ($content_bottom): ?><div id="content-bottom"><?php print $content_bottom; ?></div><?php endif; ?>

      </div>
    </div>

  </div>

</div>
</div>
</div>

    <?php if ($sidebar_left):?>
    <div id="sidebar-left">
      <div id="leftsidebar" class="sidebar">
        <div class="textpadder">
         <?php print $sidebar_left; ?>
        </div>
       </div>
    </div>
    <?php endif; ?>
    <br class="brclear" />

</div>

    <?php if ($sidebar_right):?>
    <div id="sidebar-right">
      <div id="rightsidebar" class="sidebar">
        <div class="textpadder">
         <?php print $sidebar_right; ?>
        </div>
      </div>
    </div>
    <?php endif; ?>
    <br class="brclear" />

</div>

<?php if ($sidebar_right):?>
</div>
<?php endif; ?>

<?php if ($sidebar_left):?>
</div>
<?php endif; ?>

</div>

<div class="footer">
    <div id="footer-wrapper">
      <?php if ($footer_message):?><div id="footer-region"><?php print $footer_message; ?>
    </div><?php endif; ?>
        <div class="footer-right">
            <div class="footer-left">
            </div>
        </div>
    </div>
</div>

<?php if ($page_bottom):?><div id="page-bottom-region"><?php print $page_bottom; ?></div><?php endif; ?>

<?php print $closure ?>

</div>
</div>
</div>
</div>

</body>
</html>
